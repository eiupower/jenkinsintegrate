CREATE TABLE token (
        id varchar(255) NOT NULL,
        expiry_datetime timestamp NOT NULL,
        token_data varchar(1024) NOT NULL,
        PRIMARY KEY (id)
    );

CREATE INDEX token_exipiry_datetime_index ON token (expiry_datetime);