create table Company (
    uuid VARCHAR(255) not null,
    bpPersonRole varchar2(255),
    complexStructure varchar2(255),
    complexStrucutreDetails varchar2(255),
    crsEntType varchar2(255),
    crsEntityType varchar2(255),
    establishDate date,
    firmName varchar2(255),
    giin varchar2(255),
    legalForm varchar2(255),
    nogaCode varchar2(255),
    passiveNfeType varchar2(255),
    reasonCMPerson varchar2(255),
    sensitivBusinessSector varchar2(255),
    w8BenE varchar2(255),
    primary key (uuid)
);

alter table Person add CompanyId VARCHAR(255);
/
DECLARE
    rand_uuid varchar2(36);
BEGIN
    FOR person_record IN (SELECT * FROM PERSON WHERE PERSONTYPEID = 'LEGAL_PERSON') LOOP
        select lower(regexp_replace(rawtohex(sys_guid()), '([A-F0-9]{8})([A-F0-9]{4})([A-F0-9]{4})([A-F0-9]{4})([A-F0-9]{12})', '\1-\2-\3-\4-\5')) INTO rand_uuid from dual;
        UPDATE PERSON SET CompanyId = rand_uuid WHERE UUID = person_record.UUID;
        INSERT INTO COMPANY (UUID, LEGALFORM, FIRMNAME, BPPERSONROLE, COMPLEXSTRUCTURE, COMPLEXSTRUCUTREDETAILS, CRSENTTYPE, CRSENTITYTYPE, ESTABLISHDATE, GIIN, NOGACODE, PASSIVENFETYPE, REASONCMPERSON, SENSITIVBUSINESSSECTOR, W8BENE) VALUES(rand_uuid, person_record.LEGALFORM, person_record.FIRMNAME, '', '', '', '', '', '', '', '', '', '', '', '');
    END LOOP;
END;
/
alter table Person drop (FirmName, LegalForm);

alter table Person 
    add constraint FK_1ro67fwtkqx3u5nxcyafa5f 
    foreign key (CompanyId) 
    references Company;

commit;