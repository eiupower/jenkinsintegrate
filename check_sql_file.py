# !/usr/bin/env python
MINIMUM_CHARS = 15
MAXIMUM_CHARS = 30
START_WHITELIST = '@sqllint::whitelist'
END_WHITELIST = '@sqllint::whitelist-end'
WHITELIST_REASON = '@sqllint::whitelist_reason'


def main():
    file_paths = []
    print('test Read environment')
    list_change = os.environ['LIST_CHANGE']
    temp = []
    for path in list_change.split('\n'):
        temp.append(path)
        if '.sql' in path.lower():
            file_paths.append(path)

    print('[INFO] Check sql script files')
    print(file_paths)
    print(temp)
    warning_messages = {}
    for file_path in file_paths:
    	if os.path.exists(file_path):
        	check_file(file_path, warning_messages)
    hasError = False
    for key in warning_messages:
        if len(warning_messages[key]) != 0:
            print(key)
            print(warning_messages[key])
            print('\n')
            hasError = True
    if hasError:
        sys.exit(1)
    sys.exit(0)


def check_file(filePath, warning_messages):
    file = open(filePath, "r")
    has_commit = False
    keys_check_leng = ['table', 'add', 'index', 'drop']
    file_name = os.path.basename(file.name)
    warning_messages[file_name] = []
    has_start_whitelist = False
    has_end_whitelist = False
    wrong_syntax_whitelist = False
    if file.mode == "r":
        line_number = 1
        lines = file.read().split('\n')
        for line in lines:
            line_number += 1
            line_lower = line.lower()
            line_arr = line_lower.split()
            # Start to check syntax of whitelist
            if START_WHITELIST in line_arr:
                if has_start_whitelist:
                    wrong_syntax_whitelist = True
                    break
                has_start_whitelist = True
                has_end_whitelist = False

            if END_WHITELIST in line_arr:
                if not has_start_whitelist:
                    wrong_syntax_whitelist = True
                    break
                has_end_whitelist = True
                has_start_whitelist = False

            if WHITELIST_REASON in line_lower:
                reason_content = get_text_between_parenthesis(line_lower)
                if len(reason_content) <= MINIMUM_CHARS and has_start_whitelist and not has_end_whitelist:
                    wrong_syntax_whitelist = True
                    break
            # Start to check rules for sql script follow page: https://ubitecag.atlassian.net/wiki/spaces/CIC/pages/1320518093/Database+Development+Life+Cycle
            if not has_start_whitelist:
                if 'drop' in line_arr:
                    warning_messages[file_name].append('File contain drop syntax in line: ' + str(line_number))

                if not line.isascii():
                    warning_messages[file_name].append('File contain weird characters in line: ' + str(line_number))
                check_length(file_name, keys_check_leng, line_number, line_arr, warning_messages)

            if 'commit' in line_lower:
                has_commit = True

        if wrong_syntax_whitelist:
            warning_messages[file_name] = 'ERROR Syntax of whitelist \n ' \
                                          'The whitelist should be: \n \t -- @sqllint::whitelist' \
                                          '\n \t -- @sqllint::whitelist_reason(${reasonContent})' \
                                          '\n \t ${sql_script}'\
                                          '\n \t -- @sqllint::whitelist-end \n' \
                                          'NOTE:' \
                                          '\n \t + Do not accept nested whitelist.' \
                                          '\n \t + Reason must have content and at least 15 characters.'
        elif not has_commit:
            warning_messages[file_name].append('File does not have commit')


def get_text_between_parenthesis(line_lower):
    return line_lower[line_lower.find('(') + 1:line_lower.find(')')]


def check_length(fileName, keys_check_leng, lineNumber, line_arr, warningFile):
    for key in keys_check_leng:
        if key in line_arr:
            for word in line_arr:
                if len(word) > MAXIMUM_CHARS:
                    warningFile[fileName].append(
                        'File contain name/index over 30 characters in line: ' + str(lineNumber))


if __name__ == "__main__":
    import os
    import sys

    main()
